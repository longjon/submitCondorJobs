#!/bin/bash 

# The shebang is important!  Don't forget it.

workingDir=${1}
jobName=${2}

# You can also setup jobs to run on the condor node's space
cd $workingDir

## Setup Environment
# Root
export ALRB_localConfigDir="/etc/hepix/sh/GROUP/zp/alrb"
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh ""

source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh "views LCG_94 x86_64-centos7-gcc62-opt"
source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh "root 6.10.06-x86_64-slc6-gcc62-opt"

# Athena
#athena_version="Athena,22.0.33"
#export ALRB_localConfigDir="/etc/hepix/sh/GROUP/zp/alrb";
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh;
#source ${AtlasSetup}/scripts/asetup.sh ${athena_version}

# Don't forget to source your build/x86_64-*/setup.sh if you have local modifications
# And you'll probably need to run asetup in the TestArea to get everything working

# Run code
echo $jobName
echo $workingDir

