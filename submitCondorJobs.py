#!/usr/bin/env python3

"""
Generic script for submitting batch jobs to condor on lxplus.
Makes directories in ./logs/ per job and submits from that directory.

April 6th, 2020
Jonathan Long
"""


import os
import argparse
import subprocess as sp
from pathlib import Path
import shutil
from datetime import datetime

########## Settings ################### 
parser = argparse.ArgumentParser()
parser.add_argument("--dryRun",   help="Don't actually submit job", action="store_true")
#key

args = parser.parse_args()

dryRun = False
if args.dryRun:
    dryRun = True


jobScript = "job.sh" # bash script that gets run on batch system

currentDir = Path.cwd()
logDir = currentDir / "logs"

jobNamePrefix = ""



############ Deeper Settings ###############
timeNow = datetime.now()

if not logDir.exists():
    print("Creating log directory: ",logDir)
    Path.mkdir(logDir)

    

#################################################
# Define something to loop over that will be your jobs
basedir="/eos/atlas/user/l/longjon/Trigger/"
listOfJobs = ["data22_13p6TeV.00427514.physics_Main.daq.RAW"] 


for job in listOfJobs:

    # Setup Job Name
    jobName = (jobNamePrefix + "_" if jobNamePrefix!="" else "") + job + "_" + str(timeNow.year) + "-" + str(timeNow.month).zfill(2) + "-" + str(timeNow.day).zfill(2) #+ "-" + str(timeNow.hour).zfill(2) + "-" + str(timeNow.minute).zfill(2) + "-" + str(timeNow.second).zfill(2)

    #Files = sp.check_output(f"ls {basedir}/{job}", shell=True).decode('UTF-8').split()
    #nFiles = len(Files)

    jobDir = logDir / jobName
    if not jobDir.exists():
        print("Creating job directory: ", jobDir)
        Path.mkdir(jobDir)

    os.chdir(jobDir)    

    ### Copy job script to jobdir
    shutil.copy(currentDir / jobScript, jobDir / jobScript)
    
    ### Make condor file
    #espresso     = 20 minutes
    #microcentury = 1 hour
    #longlunch    = 2 hours
    #workday      = 8 hours
    #tomorrow     = 1 day
    #testmatch    = 3 days
    #nextweek     = 1 week

    condorFileName = str(jobDir / (jobName+".condor"))
    condorFile = open(condorFileName ,"w")
    condorFile.write(
f"""
executable      = {jobScript}
universe        = vanilla
arguments       = "{jobDir} {job} $(ProcID)"

output          = {jobDir}/job.$(ProcID).out
error           = {jobDir}/job.$(ProcID).err
log             = {jobDir}/job.$(ProcID).log

#send_credential = True
#+JobFlavour = "longlunch"
Queue
"""
)
    #Queue {nFiles}
    condorFile.close()

    ### Submit job
    if not dryRun:
        print(str(sp.check_output("condor_submit " + condorFileName, shell=True, encoding="ASCII")))
    



print("I'm finished!")

