# Simple shell setup to submit condor jobs on lxplus
`job.sh` should run whatever you want to do.  `submitCondorJobs.py` creates a log directory and generates the submission file on-the-fly.  Setup your listOfJobs to loop through there as well as the queue to go into, depending on the expected walltime of the job.


In the submission file (*.condor), $(ProcId) is useful to differentiate jobs if more than one is queued from the same file.  `simple_example` contains a basic setup with some other more useful features like transfering files to the node and back.

# Environment setup   

alias, which, and type are useful to figure out the full paths of various commands, e.g. asetup