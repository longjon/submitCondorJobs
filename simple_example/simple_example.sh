#!/bin/bash 

# The shebag is important!  Don't forget it.
jobID=${1}



echo "I'm job number ${jobID} running at"
pwd

echo "Hello from the node" >> exampleReturnFile.txt

echo "Here is my input data:"
cat simple_example.dat
echo "All of the local files: "
ls

echo "I'm finished"
